#include <stdio.h>
#include <stdlib.h>

int binary_search(int *array, int length, int search)
{
    int middle = length / 2;
    int offset = length % 2;
    if (middle > 0) {
        int *first = array;
        int *second = array + middle;
        if (search == array[middle]) {
            return 1;
        } else if (search < array[middle]) {
            return binary_search(first, middle - offset, search);
        }
        return binary_search(second, middle + offset, search);
    }
    return (array[middle] == search) ? 1 : 0;
}

int main(int argc, char *argv[])
{
    int array[9] = {1, 3, 4, 5, 8, 12, 15, 47, 49};
    int length = sizeof array / sizeof(array[0]);
    return binary_search(array, length, atoi(argv[1]));
}
