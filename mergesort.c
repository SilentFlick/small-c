#include <stdio.h>
#include <stdlib.h>

void merge(int *a, int l, int r, int n)
{
    int *b = (int *)malloc(n * sizeof(int));
    int  p1 = l;
    int  p2 = ((r + l) / 2) + 1;
    int  c = l;
    while ((p1 < ((l + r) / 2) + 1) && (p2 < r + 1)) {
        if (a[p1] <= a[p2]) {
            b[c++] = a[p1];
            p1++;
        } else {
            b[c++] = a[p2];
            p2++;
        }
    }

    if (p2 == r + 1) {
        while ((p1 < ((l + r) / 2) + 1)) {
            b[c++] = a[p1];
            p1++;
        }
    } else {
        while ((p2 < r + 1)) {
            b[c++] = a[p2];
            p2++;
        }
    }
    for (c = l; c < r + 1; c++)
        a[c] = b[c];
    free(b);
}

void swap(int *a, int *b)
{
    int t;
    t = *a;
    *a = *b;
    *b = t;
}
void merge_sort(int *a, int n, int l, int r)
{
    if (r - l == 1) {
        if (a[l] > a[r])
            swap(&a[l], &a[r]);
    } else if (l != r) {
        merge_sort(a, n, l, (l + r) / 2);
        merge_sort(a, n, ((l + r) / 2) + 1, r);
        merge(a, l, r, n);
    }
}

int main(void)
{
    int *a, n, i;
    scanf("%d", &n);
    a = (int *)malloc(n * sizeof(int));
    for (i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }

    merge_sort(a, n, 0, n - 1);
    for (i = 0; i < n; i++) {
        printf(" %d", a[i]);
    }

    free(a);

    return 0;
}
