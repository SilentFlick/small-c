#include <stdio.h>

int dfa(int start, int (*transFunc)[2], int end, char *input)
{
    char *pointer;
    int   state = start;
    for (pointer = input; *pointer != '\0'; pointer++) {
        int c = *pointer - '0';
        if (c > 1 || c < 0) {
            return 0;
        }
        state = transFunc[state][c];
    }
    if (state == end) {
        return 1;
    }
    return 0;
}

int main(void)
{
    char *input1 = "010011100101"; // true
    char *input2 = "01011";        // false
    char *input3 = "01110101";     // true
    int   states[5][2] = {
        {1, 0},
        {1, 2},
        {3, 0},
        {1, 4},
        {1, 0}
    };

    int res = dfa(0, states, 4, input1);
    printf("%s --> %d\n", input1, res);
    res = dfa(0, states, 4, input2);
    printf("%s --> %d\n", input2, res);
    res = dfa(0, states, 4, input3);
    printf("%s --> %d\n", input3, res);
    return 0;
}
