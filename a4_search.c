#include <ctype.h>
#include <stdio.h>
#include <string.h>

// return non-zero if text contains word.
int search(char *w, char *t)
{
    char *cursor = w;
    while (*t) {
        // compare and move both pointers to the next character
        if (*(t++) == *(cursor++)) {
            // If pointer to text (t) is not letter or number, that means we
            // just finish comparing a word from text and the search word.
            // Therefore, if cursor is also at eol, the two compared words are
            // equal.
            if (*cursor == 0 && isalnum(*t) == 0)
                return 1;
        } else {
            cursor = w;
        }
    }
    return 0;
}

// print location of the word in text, and return the number found words.
int contain(char *w, char *t)
{
    char *pw = w;
    char *pt = t;
    int   count = 0;
    while (*pt) {
        if (*(pt++) == *(pw++)) {
            if (*pw == 0 && isalnum(*pt) == 0) {
                int index = pt - t;
                int offset = pw - w;
                printf("The word is found at position: %d\n", index - offset);
                pw = w;
                count++;
            }
        } else {
            pw = w;
        }
    }
    return count;
}

int main(void)
{
    char *t = "this is a line of words. this is a line of words!";
    char *w = "words";
    int   ret = contain(w, t);
    printf("Number of words: %d\n", ret);
    return 0;
}
