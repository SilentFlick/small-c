#include <stdlib.h>

typedef struct btree {
    int           value;
    struct btree *left;
    struct btree *right;
} btree;

int find(btree *root, int searchValue)
{
    if (root == NULL) {
        return 0;
    } else if (searchValue > root->value) {
        return find(root->right, searchValue);
    } else if (searchValue < root->value) {
        return find(root->left, searchValue);
    } else if (searchValue == root->value) {
        return 1;
    }
    return 0;
}
